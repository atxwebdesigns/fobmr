<?php
/**
 * Template list all freelancer current bid
 # This template is load page-profile.php
 * @since 1.0
*/
global $wp_query, $ae_post_factory;
$post_object = $ae_post_factory->get( BID );
?>
<div class="">
    <ul class="list-user-bid-container">
        <?php
            $postdata = array();
            if(have_posts()){
                while (have_posts()) { the_post();
                    $convert    = $post_object->convert($post);
                    $postdata[] = $convert;
                    get_template_part( 'mobile/template/user', 'bid-item' );
                }
            } else {
                echo '<li><div class="no-results no-padding-top">'.__('<p>You have not bidded any projects yet.</p><p>If you already finished updating your profile, it is time to start bidding.</p><div class="add-project"><a href="'.get_post_type_archive_link( PROJECT ).'" class="btn-submit-project">Find a project</a></div>', ET_DOMAIN).'</div></li>';
            }
        ?>
    </ul>
</div>
 <!-- </ul> -->
<?php
    if(have_posts()){
        echo '<div class="paginations-wrapper">';
        ae_pagination($wp_query, get_query_var('paged'), 'load');
        echo '</div>';
        /**
         * render post data for js
        */
        echo '<script type="data/json" class="postdata" >'.json_encode($postdata).'</script>';
    }
?>