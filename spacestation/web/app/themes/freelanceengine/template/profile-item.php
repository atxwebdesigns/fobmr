<?php
/**
 * The template for displaying profile in a loop
 * @since  1.0
 * @package FreelanceEngine
 * @category Template
 */
global $wp_query, $ae_post_factory, $post,$current_user, $user_ID;
$post_object = $ae_post_factory->get( PROFILE );
$current = $post_object->current_post;
if(!$current){
	return;
}
$user_role = ae_user_role($current_user->ID);
//print_r($current);
 $post_object1 = $ae_post_factory->get(PROFILE);
	  $profile_id = get_user_meta( $current->post_author, 'user_profile_id', true);
   $profile1 = array('id' => 0, 'ID' => 0);
	if($profile_id) {
		$profile_post = get_post( $profile_id );
		if($profile_post && !is_wp_error( $profile_post )){
			$profile1 = $post_object1->convert($profile_post);

		}
	}
?>
<div <?php post_class( 'col-md-6 col-sm-12 col-xs-12 profile-item' ); ?>>
	<div class="profile-content">
		<ul class="top-profile">
			<li class="img-avatar">
				<span class="avatar-profile">
					<?php echo get_avatar($post->post_author); ?>
				</span>
			</li>
			<li class="info-profile">
				<a href="<?php echo get_author_posts_url( $post->post_author ); ?>">
					<span class="name-profile"><?php //the_author_meta( 'display_name', $post->post_author );
					echo $profile1->service_garage;  ?></span>
				</a>
				<div class="rate-it" data-score="<?php echo $current->rating_score; ?>"></div>
			</li>
		</ul>
		<div class="bottom-profile">
			<div class="address">
				<?php if ($profile1->city) {
					if ($profile1->address) {
						echo $profile1->address . '<br>';
					}
					echo $profile1->city . ', TX ' . $profile1->zip;
				} ?>
			</div>
			<a href="<?php echo $current->permalink; ?>" class="btn btn-view-profile" title="<?php _e('View Profile', ET_DOMAIN);?>">
				View Profile
			</a>
		</div>
	</div>
</div>
