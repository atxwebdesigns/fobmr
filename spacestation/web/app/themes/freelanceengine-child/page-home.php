<?php
/**
 * Template Name: Front Page Template
*/
global $wp_query, $ae_post_factory, $post;
get_header();
?>

<div class="vert-grid">
	<span class="vert-grid__line"></span>
	<span class="vert-grid__line"></span>
	<span class="vert-grid__line"></span>
	<span class="vert-grid__line"></span>
	<span class="vert-grid__line"></span>
	<span class="vert-grid__line"></span>
</div>

<section class="hero">
	<div class="hero__sector01">
		<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/driving.jpg');"></div>
		<video loop="" muted="" autoplay="" poster="<?php bloginfo("template_url"); ?>-child/dist/images/driving.jpg">
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/driving.mp4" type="video/mp4"/>
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/driving.webm" type="video/webm"/>
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/driving.ogv" type="video/ogg"/>
		</video>
	</div>
	<div class="hero__sector02">
		<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/bling_blink.jpg');"></div>
		<video loop="" muted="" autoplay="" poster="<?php bloginfo("template_url"); ?>-child/dist/images/bling_blink.jpg">
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.mp4" type="video/mp4"/>
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.webm" type="video/webm"/>
			<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.ogv" type="video/ogg"/>
		</video>
	</div>
	<div class="content">
		<div class="content__sector01">
			<h2>
				<div class="heading-divider"></div>
				Fix or Build?
			</h2>
			<a href="<?= esc_url(home_url('/')); ?>post-your-repair" class="button-diagonal">
				<div class="button-backing"></div>
				<span class="btn-label">Post a Repair</span>
			</a>
		</div>
		<div class="content__sector02">
			<h2>
				<div class="heading-divider"></div>
				Bid &amp; Repair?
			</h2>
			<a href="<?= esc_url(home_url('/')); ?>repairs" class="button-diagonal">
				<div class="button-backing"></div>
				<span class="btn-label">View Repair Jobs</span>
			</a>
		</div>
	</div>

	<div class="hero__footer">
		<div class="down">
			<a href="#scroll-content" class="page-down">
				<span class="down-arrow"></span>
				<span class="down-arrow"></span>
			</a>
		</div>
	</div>
</section>

<secton id="scroll-content" class="how-does-it-work">
	<div class="vert-grid">
		<span class="vert-grid__line"></span>
		<span class="vert-grid__line"></span>
		<span class="vert-grid__line"></span>
		<span class="vert-grid__line"></span>
		<span class="vert-grid__line"></span>
		<span class="vert-grid__line"></span>
	</div>

	<div class="diagshape"></div>

	<div class="floating-box">
		<a href="#play-video" class="video-link">
			<span class="play">
				<span class="icon"></span>
			</span>
		</a>
		<div class="open-video">
			<a href="#close" class="close"></a>
			<div class="vid-container">
				<!-- Video -->
				<div class="js-player" data-type="youtube" data-video-id="gDCHGzAn6pk"></div>
			</div>
		</div>
		<div class="info">
			<div class="side-text">Fix or Build My Ride</div>
			<div class="content">
				<div class="heading">
					<div class="info-icon"></div>
					<div class="divider"></div>
					How Does It Work?
				</div>
				<div class="description">
					<p>The intent of the FixorBuildMyRide (FOBMR) site is to connect people who need service to a vehicle with vehicle-service professionals who compete online for the work. The FOBMR Team is comprised of auto enthusiasts, gear heads, and tech nerds who Developed the FOBMR site for the Auto Service Professionals to bid on service requests posted by registered FOBMR customers. The FOBMR Team understand not all customers know the difference between a piston and a fuel injector, so we make it easy for everyone to post service requests ranging from general diagnostic work to giving us detailed work instructions.</p>

					<p>The use of the site is simple and free to customers who register, post a service request, formally accept a bid, schedule service with the shop, and rate the experience when the work is completed. FOBMR also makes it easy for vehicle-service professionals to register and subscribe to our service-request monitoring and bidding features, competing for work for which they are qualified.</p>
				</div>
			</div>
		</div>
	</div>
</secton>

<section class="home-content">

<?php
if(have_posts()) {
	the_post();
		if (!is_user_logged_in()){
			// Guest View
			?>
			<div class="slider-switch">
				<a href="#customer-steps" class="switcher switcher--active sswitch-customer">
					<span class="wrapper">
						<svg class="sswitch-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96"><path d="M64.7 53.3C69.2 48.9 72 42.8 72 36V24C72 10.7 61.3 0 48 0S24 10.7 24 24v12c0 6.8 2.8 12.9 7.3 17.3C13 56.1 0 63.5 0 72.1V80c0 6.6 5.4 12 12 12h72c6.6 0 12-5.4 12-12v-7.9c0-8.6-13-16-31.3-18.8zM32 36V24c0-8.8 7.2-16 16-16s16 7.2 16 16v12c0 8.8-7.2 16-16 16s-16-7.2-16-16zm56 44c0 2.2-1.8 4-4 4H12c-2.2 0-4-1.8-4-4v-7.9c0-.9 2.3-3.9 9.2-6.8 8-3.4 19.2-5.3 30.8-5.3 11.6 0 22.8 1.9 30.8 5.3 6.9 2.9 9.2 5.9 9.2 6.8V80z"/></svg>
						<span>Customer</span>
					</span>
				</a>
				<a href="#shop-steps" class="switcher sswitch-shop">
					<span class="wrapper">
						<svg class="sswitch-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96"><path d="M84,60c6.6,0,12-5.4,12-12s-5.4-12-12-12h-2c-0.4-1.2-0.9-2.4-1.5-3.5l1.5-1.4c4.7-4.7,4.7-12.3,0-17 c-4.7-4.7-12.3-4.7-17,0l-1.4,1.5c-1.1-0.5-2.3-1-3.5-1.5v-2c0-6.6-5.4-12-12-12S36,5.4,36,12v2c-1.2,0.4-2.4,0.9-3.5,1.5L31,14.1 c-4.7-4.7-12.3-4.7-17,0c-4.7,4.7-4.7,12.3,0,17l1.5,1.4c-0.5,1.1-1,2.3-1.5,3.5h-2C5.4,36,0,41.4,0,48s5.4,12,12,12h2 c0.4,1.2,0.9,2.4,1.5,3.5L14.1,65c-4.7,4.7-4.7,12.3,0,17c4.7,4.7,12.3,4.7,17,0l1.4-1.5c1.1,0.5,2.3,1,3.5,1.5v2 c0,6.6,5.4,12,12,12s12-5.4,12-12v-2c1.2-0.4,2.4-0.9,3.5-1.5l1.4,1.5c4.7,4.7,12.3,4.7,17,0c4.7-4.7,4.7-12.3,0-17l-1.5-1.4 c0.5-1.1,1-2.3,1.5-3.5H84z M75.7,52c-0.7,4.7-2.5,9.1-5.3,12.8l5.7,5.7l0.2,0.2c1.6,1.6,1.6,4.1,0,5.7s-4.1,1.6-5.7,0l-0.2-0.2 l-5.7-5.7c-3.7,2.7-8,4.6-12.8,5.3V84c0,2.2-1.8,4-4,4s-4-1.8-4-4v-8.3c-4.7-0.7-9.1-2.5-12.8-5.3l-5.7,5.7l-0.2,0.2 c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7l0.2-0.2l5.7-5.7c-2.7-3.7-4.6-8-5.3-12.8H12c-2.2,0-4-1.8-4-4s1.8-4,4-4h8.3 c0.7-4.7,2.5-9.1,5.3-12.8l-5.7-5.7l-0.2-0.2c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0l0.2,0.2l5.7,5.7c3.7-2.7,8-4.6,12.8-5.3 V12c0-2.2,1.8-4,4-4s4,1.8,4,4v8.3c4.7,0.7,9.1,2.5,12.8,5.3l5.7-5.7l0.2-0.2c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7l-0.2,0.2 l-5.7,5.7c2.7,3.7,4.6,8,5.3,12.8H84c2.2,0,4,1.8,4,4s-1.8,4-4,4H75.7z M48,32c-8.8,0-16,7.2-16,16s7.2,16,16,16s16-7.2,16-16 S56.8,32,48,32z M48,56c-4.4,0-8-3.6-8-8s3.6-8,8-8s8,3.6,8,8S52.4,56,48,56z"/></svg>
						<span>Shop</span>
					</span>
				</a>
			</div>
			<div class="slider-wrapper">
				<!-- Customer Slider -->
				<div id="slider-customer" class="hiw-slider visible">
					<div class="square__red"></div>
					<div class="square__blue"></div>
					<div class="text-container">
						<div class="slide-counter">
							<div class="current-wrapper">
								<div class="current visible" data-slide-ref="slide-0">01</div>
								<div class="current" data-slide-ref="slide-1">02</div>
							</div>
							<div class="total">02</div>
						</div>
						<div class="text-field visible" data-slide-ref="slide-0">
							<h4 class="title">Why Join FOBMR</h4>
							<ul>
								<li>Save time by not having to call multiple mechanic shops </li>
								<li>Find the best service for the best price near you</li>
								<li>Keep track of all repairs and maintenance done to your vehicle </li>
								<li>Feel secure knowing that your repairs are completed by reputable mechanics</li>
								<li>Quickly compare garages on price, location and customer feedback.</li>
							</ul>
							<a href="<?= esc_url(home_url('/')); ?>post-your-repair" class="button nav-link">
								<div class="bottom"></div>
								<div class="top">
								<div class="label">Post Your Repair Now!</div>
								<div class="button-border button-border-left"></div>
								<div class="button-border button-border-top"></div>
								<div class="button-border button-border-right"></div>
								<div class="button-border button-border-bottom"></div>
								</div>
							</a>
						</div>
						<div class="text-field" data-slide-ref="slide-1">
							<h4 class="title">How to Get Started</h4>
							<ol>
								<li>Signup to create an account</li>
								<li>Post your repair</li>
								<li>Accept a mechanics bid</li>
							</ol>
							<p class="ctatext">After your repair is complete you'll acknowledge what was done and leave your feedback for that shop. Why wait?</p>
							<a href="<?= esc_url(home_url('/')); ?>post-your-repair" class="button nav-link">
								<div class="bottom"></div>
								<div class="top">
								<div class="label">Post Your Repair Now!</div>
								<div class="button-border button-border-left"></div>
								<div class="button-border button-border-top"></div>
								<div class="button-border button-border-right"></div>
								<div class="button-border button-border-bottom"></div>
							</div>
							</a>
						</div>
					</div>
					<ul class="image-container">
						<li class="slide visible" data-slide-ref="slide-0">
							<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/step-slider/customer-step-01.jpg');"></div>
						</li>
						<li class="slide" data-slide-ref="slide-1">
							<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/step-slider/customer-step-02.jpg');"></div>
						</li>
					</ul>
					<div class="slider-controls">
						<a href="#next" class="controler __next">
							<span class="label">Next</span>
						</a>
						<a href="#prev" class="controler __prev">
							<span class="label">Prev</span>
						</a>
					</div>
				</div>
				<!-- Shop Slider -->
				<div id="slider-shop" class="hiw-slider">
					<div class="square__red"></div>
					<div class="square__blue"></div>
					<div class="text-container">
						<div class="slide-counter">
							<div class="current-wrapper">
								<div class="current visible" data-slide-ref="slide-0">01</div>
								<div class="current" data-slide-ref="slide-1">02</div>
							</div>
							<div class="total">02</div>
						</div>
						<div class="text-field visible" data-slide-ref="slide-0">
							<h4 class="title">Why Join FOBMR</h4>
							<ul>
								<li>Find customers needing your services in one place</li>
								<li>Save time from phone calls of customers "shopping around" </li>
								<li>Fill your bays, increase profit and reduce downtime</li>
								<li>Gain new customers who you may retain for years to come </li>
								<li>Free unlimited bidding, and only pay for jobs won</li>
							</ul>
							<a href="<?= esc_url(home_url('/')); ?>repairs" class="button nav-link">
								<div class="bottom"></div>
								<div class="top">
								<div class="label">Start Bidding On Repairs!</div>
								<div class="button-border button-border-left"></div>
								<div class="button-border button-border-top"></div>
								<div class="button-border button-border-right"></div>
								<div class="button-border button-border-bottom"></div>
								</div>
							</a>
						</div>
						<div class="text-field" data-slide-ref="slide-1">
							<h4 class="title">How to Get Started</h4>
							<ol>
								<li>Signup to create an account</li>
								<li>Complete your profile</li>
								<li>Begin bidding on posted repairs</li>
							</ol>
							<p class="ctatext">After you've won and completed a customer's repair, you'll acknowledge what it was repaired and leave you feedback for that customer. Why Wait?</p>
							<a href="<?= esc_url(home_url('/')); ?>repairs" class="button nav-link">
								<div class="bottom"></div>
								<div class="top">
								<div class="label">Start Bidding On Repairs!</div>
								<div class="button-border button-border-left"></div>
								<div class="button-border button-border-top"></div>
								<div class="button-border button-border-right"></div>
								<div class="button-border button-border-bottom"></div>
								</div>
							</a>
						</div>
					</div>
					<ul class="image-container">
						<li class="slide visible" data-slide-ref="slide-0">
							<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/step-slider/customer-step-01.jpg');"></div>
						</li>
						<li class="slide" data-slide-ref="slide-1">
							<div class="image" style="background-image: url('<?php bloginfo("template_url"); ?>-child/dist/images/step-slider/customer-step-02.jpg');"></div>
						</li>
					</ul>
					<div class="slider-controls">
						<a href="#next" class="controler __next">
							<span class="label">Next</span>
						</a>
						<a href="#prev" class="controler __prev">
							<span class="label">Prev</span>
						</a>
					</div>
				</div>
			</div>
		<?php
		} else { // Logged In
			// SHOP
			if (ae_user_role() == FREELANCER) { ?>
				<div class="content-title">
					<div class="content-title__subtitle">
						<div class="subtitle__line"></div>
						<div class="subtitle__title">Looking for Repair Jobs?</div>
					</div>
					<h3 class="content-title__primary">Recent Jobs</h3>
				</div>
				<?php
				$the_query = new WP_Query( 'page_id=655' );

				while ($the_query -> have_posts()) : $the_query -> the_post();
					the_content();
				endwhile;

				wp_reset_query();
			// CUSTOMER
			} else { ?>
				<div class="content-title">
					<div class="content-title__subtitle">
						<div class="subtitle__line"></div>
						<div class="subtitle__title">Looking to Invite a Shop?</div>
					</div>
					<h3 class="content-title__primary">Recent Shops</h3>
				</div>
				<?php
				$the_query = new WP_Query( 'page_id=659' );

				while ($the_query -> have_posts()) : $the_query -> the_post();
					the_content();
				endwhile;

				wp_reset_query();
			}
		}

		// Jobs
		/*if (ae_user_role() == FREELANCER) {
			$the_query = new WP_Query( 'page_id=655' );

			while ($the_query -> have_posts()) : $the_query -> the_post();
				the_content();
			endwhile;

			wp_reset_query();

		// Shops
		} elseif (is_user_logged_in()) {
			$the_query = new WP_Query( 'page_id=659' );

			while ($the_query -> have_posts()) : $the_query -> the_post();
				the_content();
			endwhile;

		  wp_reset_query();
		}*/
}
?>
</section>
<?php
get_footer();
