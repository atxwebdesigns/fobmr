<?php
/**
 * Template Name: Splash Page Template
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>-child/dist/css/main.css">
	</head>

	<body class="page-template-page-splash">
		<div class="hero-video">
			<video class="hero-video__video" loop="" muted="" autoplay="" poster="<?php bloginfo("template_url"); ?>-child/dist/images/bling_blink.jpg">
				<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.mp4" type="video/mp4"/>
				<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.webm" type="video/webm"/>
				<source src="<?php bloginfo("template_url"); ?>-child/dist/video/bling_blink.ogv" type="video/ogg"/>
			</video>
			<div class="graphics">
				<div class="top-slice"></div>
				<div class="bottom-slice"></div>
				<div class="frame"></div>
			</div>
			<div class="content">
				<img src="<?php bloginfo("template_url"); ?>-child/dist/images/logo.png" class="logo" alt="FOBMR">
				<h4>Fix or Build My Ride</h4>
				<h1>Coming Soon!</h1>
			</div>
		</div>
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
	<script src="<?php bloginfo("template_url"); ?>-child/dist/js/main.js"></script>
	</body>
</html>
