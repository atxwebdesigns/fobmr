<?php
add_filter( 'cron_schedules', 'myprefix_add_weekly_cron_schedule' );
function myprefix_add_weekly_cron_schedule( $schedules ) {
    $schedules['weekly'] = array(
        'interval' => 86400, // 1 week in seconds
        'display'  => __( 'Once Weekly' ),
    );

    return $schedules;
}
function foobar_truncate_posts(){
    global $wpdb;

    # Set your threshold of max posts and post_type name
    $threshold = 50;
    $post_type = 'foobar';
    $todaydate=time()-(60*60*24*7);
    # Query post type
    $query = "
        SELECT ID FROM $wpdb->posts
        WHERE post_type = 'project'
        AND post_status = 'publish' AND post_date < $todaydate
        ORDER BY post_modified DESC
    ";
    $results = $wpdb->get_results($query);

    # Check if there are any results
    if(count($results)){
        foreach($result as $post){
            $i++;

            # Skip any posts within our threshold
            if($i <= $threshold)
                continue;

            # Let the WordPress API do the heavy lifting for cleaning up entire post trails
            $purge = wp_delete_post($post->ID);
        }
    }
}
if(!wp_next_scheduled( 'foobar_truncate_posts_schedule')){
    wp_schedule_event(time(), 'weekly', 'foobar_truncate_posts_schedule');
}
add_action('foobar_truncate_posts_schedule', 'foobar_truncate_posts');

add_action( 'admin_post_add_foobar', 'prefix_admin_add_foobar' );
//this next action version allows users not logged in to submit requests

//if you want to have both logged in and not logged in users submitting, you have to add both actions!

add_action( 'admin_post_nopriv_add_foobar', 'prefix_admin_add_foobar' );


function prefix_admin_add_foobar() {
    global $current_user;
$ae_users  = AE_Users::get_instance();
 $user_data = $ae_users->convert($current_user->data);
  if(isset($_POST['delteaccount']))
         {
           // echo 'hel'.$_POST['userid'];
         if(wp_delete_user( $_POST['user_delete_me'] ))
         {
            // echo 'hi';
                $headers[] = 'From: Me Myself <admin@atxclients.com>';
         if(wp_mail($user_data->user_email,'Account Deletion Confirmation', "Your Account is Deleted From FOBMR",$headers))
         {
             echo "You are deleted, Confirmation mail is sent your email id";
            wp_redirect( $_POST['redirect_url'] . '?deleted=true' );
         }
         }
         }
}
