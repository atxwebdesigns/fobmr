<?php
/**
 * The template for display a list bids of a project
 * @since 1.0
 * @author Dakachi
 */
global $wp_query, $ae_post_factory, $post,$current_user, $user_ID;
 //yglobal $wp_query, $ae_post_factory, $post, $current_user, $user_ID;
$post_object    = $ae_post_factory->get(PROJECT);

$project            = $post_object->current_post;

$number_bids    = (int)get_number_bids( get_the_ID() );
//$sum            = (float) get_total_cost_bids( get_the_ID() );
add_filter('posts_orderby', 'fre_order_by_bid_status');
$user_role = ae_user_role($current_user->ID);
$q_bid      = new WP_Query( array(  'post_type' => BID,
                                    'post_parent' => get_the_ID(),
                                    'post_status' => array('publish','complete', 'accept', 'unaccept')
                                )
                            );
remove_filter('posts_orderby', 'fre_order_by_bid_status');
$biddata    = array();


?>
<div class="col-md-8">

    <div class="row title-tab-project <?php if($q_bid->found_posts < 1 ) echo 'visibility-hidden' ?>">
        <div class="col-md-7 col-xs-7">
            <span><?php printf(__('BUSINESS BIDDING (%s)',ET_DOMAIN), $number_bids);?></span>
        </div>
        <div class="col-md-5 col-xs-5 block-bid-header" >
            <span><?php _e('BID',ET_DOMAIN);?></span>
        </div>
    </div>
    <div class="info-bidding-wrapper list-bid-project project-<?php echo $project->post_status; ?> freelancer-bidding">
        <?php
if($user_role =="administrator" || $user_role =="employer") {
        if( $q_bid->have_posts() ):

            global $wp_query, $ae_post_factory, $post;
            $post_object = $ae_post_factory->get(BID);
            echo "<div class='list-bidden'>";
            while( $q_bid ->have_posts() ) :$q_bid->the_post();
                $convert    = $post_object->convert($post);
                $biddata[]  = $convert;
                get_template_part('template/bid','item');
            endwhile;
            echo "</div>";
            echo '<div class="row list-bidding-js"></div> ';
            echo '<div class="paginations-wrapper text-center padding-pagiantion">';
                $q_bid->query = array_merge(  $q_bid->query ,array('is_single' => 1 ) ) ;
                ae_pagination($q_bid, get_query_var('paged'), 'load');
            echo '</div>';

        else :
            get_template_part('template/bid','not-item');
        endif;
}
else{echo '<div class="col-md-12 col-sm-12" style="line-height:26px;padding:50px;background:#fff">'.$number_bids.' Business has bid on this repair so far<br></div>';}
    

        ?>

    </div>
</div>
<?php  ?>
<input type="hidden" id="project_id" name="<?php echo $project->ID;?>" value="<?php echo $project->ID;?>" />
<div class="col-md-4">
    <div class="row title-tab-project">
        <div class="col-md-12">
            <span><?php if($_REQUEST[workspace]!='1') {  _e('ABOUT CUSTOMER',ET_DOMAIN);} else {_e('Address Of Business',ET_DOMAIN);}?></span>
        </div>
    </div>
    <div class="info-company-wrapper">
        <div class="row">
            <div class="col-md-12">
                <?php if($_REQUEST[workspace]!='1') {fre_display_user_info_single( $project->post_author );}
                else {
                       

                     $project_object = $ae_post_factory->get(PROJECT);;
    $project = $project_object->current_post;

    $post_object    = $ae_post_factory->get( BID );
    $convert        = $post_object->convert($post);

    $bid_accept     = get_post_meta($project->ID, 'accepted', true);
    $project_status = $project->post_status;
    //print_r($convert);
   // echo $convert->post_author;
                    ?>
                <ul class="list-info-company-details">
        <li>
              <div class="address">
            <span class="addr-wrap">
                <span class="title-name"><i class="fa fa-map-marker"></i><?php _e('Address:', ET_DOMAIN); ?></span>
                <span class="info addr" title="<?php echo get_user_meta($convert->post_author, 'location', true) ?>">
                    <?php 
                     
                    echo get_user_meta($convert->post_author, 'location', true); ?>
                </span>
                </span>
            </div>
            </li></ul>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php

if(!empty($biddata)) {
    echo '<script type="data/json" class="biddata" >'.json_encode($biddata). '</script>';
}
