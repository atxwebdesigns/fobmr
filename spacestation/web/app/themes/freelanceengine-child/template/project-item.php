<?php

global $wp_query, $ae_post_factory, $post;
$post_object    = $ae_post_factory->get( PROJECT );
$current        = $post_object->current_post;
?>
<li <?php post_class( 'project-item' ); ?>>
	<a href="<?php the_permalink();?>" class="project-item-title">
		<?php the_title(); ?>
	</a>

	<div class="date">
		 <span><?php echo get_the_date() ?></span>
	</div>

	<a href="<?php the_permalink();?>" class="button-bid">
		<?php _e('Place Bid',ET_DOMAIN);?>
	</a>
</li>
