<?php 
    $step = 2;
    $disable_plan = ae_get_option('disable_plan', false);
    if($disable_plan) $step--;
?>
<div class="step-wrapper step-auth" id="step-auth">
    <a href="#" class="step-heading active">
    	<span class="number-step"><?php echo $step; ?></span>
        <span class="text-heading-step">
            <?php _e("Login or register", ET_DOMAIN); ?>
        </span>
        <i class="fa fa-caret-right"></i>
    </a>
    <div class="step-content-wrapper content  " style="<?php if($step != 1) echo "display:none;" ?>"    >
        <!-- Nav tabs-->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#signin" role="tab" data-toggle="tab"><?php _e('Login', ET_DOMAIN); ?></a></li>
            <li><a href="#signup" role="tab" data-toggle="tab"><?php _e('Register', ET_DOMAIN) ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade " id="signup">
            	<div class="text-intro-acc">
            		<?php _e('Already have an account?', ET_DOMAIN) ?>&nbsp;&nbsp;<a href="#signin" role="tab" data-toggle="tab"><?php _e('Login', ET_DOMAIN); ?></a>
                </div>

                <form role="form" id="signup_form_submit" class="signup_form_submit">
                    <input type="hidden" name="role" id="role" value="employer" />
                    <?php
                        $disable_name = apply_filters('free_register_disable_name','');
                        if(!$disable_name){
                            ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label title-plan" for="first_name"><?php _e('First name?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e("Enter first name", ET_DOMAIN) ?></span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control text-field" id="first_name" name="first_name" placeholder="<?php _e("Enter first name", ET_DOMAIN) ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label title-plan" for="last_name"><?php _e('Last name?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e("Enter last name", ET_DOMAIN) ?></span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control text-field" id="last_name" name="last_name" placeholder="<?php _e("Enter last name", ET_DOMAIN) ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                        }
                    ?>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_login"><?php _e('Username?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e('Enter username', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control text-field" id="user_login" name="user_login" placeholder="<?php _e("Enter username", ET_DOMAIN); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_email"><?php _e('Email address?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e('Enter a email', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="email" class="form-control text-field" id="user_email" name="user_email" placeholder="<?php _e("Your email address", ET_DOMAIN); ?>">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="clearfix"></div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_email"><?php _e('Request or service ?', ET_DOMAIN) ?><span><?php _e('Choose type account', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="checkbox" class="sign-up-switch" name="modal-check" data-switchery="true" style="display: none;">
                            </div>
                        </div>
                    </div> -->
                      <div class="clearfix"></div>
			                <div class="form-group">
			                	<div class="row">
			                    	<div class="col-md-4">
			                        	<label class="control-label title-plan" for="user_email"><?php _e('Customer or business?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e('Choose account type', ET_DOMAIN) ?></span></label>
			                        </div>
			                        <div class="col-sm-8">
			                        	<span class="user-type">
			                        	  <select name="role">
                                      <option value="employer">Customer</option>
                                 <option value="freelancer">Business</option>
                           </select>
				                        </span>
			                        </div>
			                    </div>
			                </div>
                    
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_pass"><?php _e('Password?', ET_DOMAIN) ?><span class="star">*</span><span><?php _e('Enter password', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="password" class="form-control text-field" id="user_pass" name="user_pass" placeholder="<?php _e('Password', ET_DOMAIN);?>">
                            </div>
                            <div class="col-md-12">    <i>Password must contain 8 character and there should be 1 lower case, 1 uppercase, at least one digit and any special character</i></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="repeat_pass"><?php _e('Retype password', ET_DOMAIN) ?><span class="star">*</span><span><?php _e('Retype password', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="password" class="form-control text-field" id="repeat_pass" name="repeat_pass" placeholder="<?php _e('Password', ET_DOMAIN);?>">
                            </div>
                        </div>
                    </div>
                    <?php if(ae_get_option('gg_captcha') && ae_get_option('gg_site_key') && !is_user_logged_in()){ ?>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-sm-8">
                                    <div class="container_captcha">
                                        <div class="gg-captcha">
                                            <?php ae_gg_recaptcha(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <?php if(get_theme_mod( 'termofuse_checkbox', false )){ ?>
                    <div class="form-group policy-agreement">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-8" >
                                <input name="agreement" id="agreement" type="checkbox" />
                                <?php printf(__('I agree with the <a href="%s" target="_Blank" rel="noopener noreferrer" >Term of use and privacy policy</a>', ET_DOMAIN), et_get_page_link('tos') ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php } ?>
                    <div class="form-group">
                    	<div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-submit btn-submit-login-form">
                                    <?php _e('Create account', ET_DOMAIN) ?>
                                </button>
                            </div>
                        </div>
                        <?php if(!get_theme_mod( 'termofuse_checkbox', false )){ ?>
                        <div class="row">
                            <div class="col-md-offset-4 col-md-8" style="margin-top:10px;">
                            <?php
                                /**
                                 * tos agreement
                                */
                                $tos = et_get_page_link('tos', array() ,false);
                                if($tos) {
                            ?>
                                <p class="text-policy">
                                    <?php printf(__('By creating an account, you agree to our <a href="%s" target="_Blank" rel="noopener noreferrer" >Term of use and privacy policy</a>', ET_DOMAIN), et_get_page_link('tos') ); ?>
                                </p>
                            <?php
                                }
                            ?>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <?php
                                if( function_exists('ae_render_social_button')){
                                    $before_string = __("You can also sign in by:", ET_DOMAIN);
                                    ae_render_social_button( array(), array(), $before_string );
                                }
                            ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade in active" id="signin">
                <div class="text-intro-acc">
            		<?php _e('Create an account', ET_DOMAIN) ?>&nbsp;&nbsp;<a href="#signup" role="tab" data-toggle="tab"><?php _e('Register', ET_DOMAIN) ?></a>
                </div>
                              
                <form role="form" id="signin_form_submit" class="signin_form_submit">
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_login"><?php _e('Username', ET_DOMAIN) ?><span><?php _e('Enter username', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control text-field" id="user_login" name="user_login" placeholder="<?php _e('Enter username', ET_DOMAIN);?>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            	<label class="control-label title-plan" for="user_pass"><?php _e('Password', ET_DOMAIN) ?><span><?php _e('Enter password', ET_DOMAIN) ?></span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="password" class="form-control text-field" id="user_pass" name="user_pass" placeholder="<?php _e('password', ET_DOMAIN);?>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-4">
                            </div>
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-submit btn-submit-login-form">
                                    <?php _e('Submit', ET_DOMAIN) ?>
                                </button>
                                  <div class="text-intro-acc">
            		<?php _e('Did you forget your password?', ET_DOMAIN) ?>&nbsp;&nbsp;<a href="#fogotpassword" role="tab" data-toggle="tab"><?php _e('Forgot password', ET_DOMAIN) ?></a>
                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                                if( function_exists('ae_render_social_button')){
                                    $before_string = __("You can also sign in by:", ET_DOMAIN);
                                    ae_render_social_button( array(), array(), $before_string );
                                }
                            ?>
                        </div>
                    </div>
                </form>
                
            </div>
            
            <div class="tab-pane fade" id="fogotpassword">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">				
				<h4 class="modal-title"><?php _e("Forgot Password?", ET_DOMAIN) ?></h4>
			</div>
			<div class="modal-body">
				<form role="form" id="forgot_form" class="auth-form forgot_form">
					<div class="form-group">
						<label for="forgot_user_email"><?php _e('Enter your email here', ET_DOMAIN) ?></label>
						<input type="text" class="form-control" id="user_email" name="user_email" />
					</div>
                    <div class="clearfix"></div>
					<button type="submit" class="btn-submit btn-sumary btn-sub-create">
						<?php _e('Send', ET_DOMAIN) ?>
					</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


        </div>
    </div>
</div>
<!-- Step 2 / End -->