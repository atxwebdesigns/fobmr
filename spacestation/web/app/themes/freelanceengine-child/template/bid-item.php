<?php
/**
 * The template for displaying a bid info item,
 * this template is used to display bid info in a project details,
 * and called at template/list-bids.php
 * @since 1.0
 * @author Dakachi
 */
    global $wp_query, $ae_post_factory,$current_user, $post, $user_ID;

    $project_object = $ae_post_factory->get(PROJECT);;
    $project = $project_object->current_post;

    $post_object    = $ae_post_factory->get( BID );
    $convert        = $post_object->convert($post);
    $bid_accept     = get_post_meta($project->ID, 'accepted', true);
    $project_status = $project->post_status;

    $post_object1 = $ae_post_factory->get(PROFILE);
      $profile_id = get_user_meta( $convert->post_author, 'user_profile_id', true);
   $profile1 = array('id' => 0, 'ID' => 0);
    if($profile_id) {
        $profile_post = get_post( $profile_id );
        if($profile_post && !is_wp_error( $profile_post )){
            $profile1 = $post_object1->convert($profile_post);
           
        }
    }
?>

<div class="row list-bidding">
    <div class="info-bidding fade-out fade-in bid-item <?php echo $convert->add_class_bid;?> bid-<?php the_ID();?> bid-item-<?php echo $project_status;?> "/> 
        <div class="col-md-7 col-xs-7">
        	<div class="avatar-freelancer-bidding"><!--<a href="<?php //echo get_author_posts_url( $convert->post_author ); ?>"><span class="avatar-profile"> <?php //echo $convert->et_avatar; ?>--></span></a></div>
            <div class="info-profile-freelancer-bidding">
                <span class="name-profile"><?php echo $profile1->service_garage; ?></span><br />
                <span class="position-profile"><?php echo $convert->et_professional_title ?></span>
                <div class="rate-exp-wrapper">
                    <div class="rate-it" data-score="<?php //echo $convert->rating_score ; ?>"></div>
                  
                </div>
                <?php if($convert->post_content){ ?>
                    <div class="comment-author-history full-text">
                        <p><?php echo $convert->post_content;?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5 col-xs-5 block-bid">
        <?php
            $time = $convert->bid_time;
            $type = $convert->type_time;
        ?>

    	<div class="number-price-project">
            <?php
            /**
             * user can view bid details
             # when a project is complete
             # when current user is project owner
             # when current user is bid owner
             */
           // if( in_array($project_status, array('complete','close', 'disputing') )
            if( ( $user_ID && $user_ID == $project->post_author ) || ( $user_ID && $user_ID == $convert->post_author ) ) { ?>
                <span class="number-price"><?php echo $convert->bid_budget_text; ?></span>
                <span class="number-day"><?php echo $convert->bid_time_text; ?></span>
            <?php } else { ?>
                <span class="number-price"><?php _e("In Process", ET_DOMAIN); ?></span>
            <?php } ?>

        </div>
        <div class="action-employer-bidden">
            <?php if( $convert->flag == 1 ){ ?>
                <?php if(ae_get_option('use_escrow')) { ?>
                    <button href="#" id="<?php the_ID();?>" rel="<?php echo $project->ID;?>" class="btn-sumary btn-accept-bid btn-bid-status"
                            title="" data-original-title="<?php _e('Accept Bid', ET_DOMAIN); ?>">
                        <?php _e('Accept',ET_DOMAIN) ; ?>
                    </button>
                <?php }else{ ?>
                    <button class="btn-sumary btn-accept-bid btn-accept-bid-no-escrow"  id="<?php the_ID();?>">
                        <?php _e('Accept',ET_DOMAIN) ;?>
                    </button>
                <?php } ?>
                <!--<span class="confirm"></span>-->
            <?php } else if( $convert->flag == 2) { ?>
                <span class="ribbon"><i class="fa fa-trophy"></i></span>
            <?php } ?>
            <?php 
                if(in_array($project_status, array('publish') )){
                    do_action('ae_bid_item_template', $convert, $project ); 
                }
            ?>
        </div>
        <?php if($convert->project_status != 'publish' && $convert->project_author == $user_ID ){ ?>
        <div class="show-info">
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
        </div>
        <?php } ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>