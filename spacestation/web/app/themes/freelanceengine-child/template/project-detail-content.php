<?php
/**
 * The template for displaying project description, comment, taxonomy and custom fields
 * @since 1.0
 * @package FreelanceEngine
 * @category Template
 */
global $wp_query, $ae_post_factory, $post, $current_user, $user_ID;
$ae_users  = AE_Users::get_instance();
$user_data = $ae_users->convert($current_user->data);
$user_role = ae_user_role($current_user->ID);

$post_object    = $ae_post_factory->get(PROJECT);
$convert = $project = $post_object->current_post;
$convert = $project = $post_object->convert($post);

$et_expired_date    = $convert->et_expired_date;
$bid_accepted       = $convert->accepted;
$project_status     = $convert->post_status;
$profile_id         = get_user_meta($post->post_author,'user_profile_id', true);

$shop_profile_id = get_user_meta( $user_ID, 'user_profile_id', true);

//convert Shop profile
$shop_post_object = $ae_post_factory->get(PROFILE);

$shop_profile = array('id' => 0, 'ID' => 0);
if($shop_profile_id) {
	$shop_profile_post = get_post( $shop_profile_id );
	if($shop_profile_post && !is_wp_error( $shop_profile_post )){
		$shop_profile = $shop_post_object->convert($shop_profile_post);
	}
}

$currency = ae_get_option('currency',array('align' => 'left', 'code' => 'USD', 'icon' => '$'));
?>
<div class="info-project-item-details">
	<div class="row">
		<div class="col-md-8">
			<div class="content-require-project">
				<h4><?php _e('Repair Description:',ET_DOMAIN);?></h4>
				<?php the_content(); ?>
				<?php do_action( 'after_single_project_content', $project); ?>

			   <div class="col-lg-12 col-md-8 col-sm-4 col-xs-5 btn-fre-bid" style="padding:0;">

					<?php
					if(current_user_can( 'manage_options' ) && $project_status != 'close') {
						get_template_part( 'template/admin', 'project-control' );
					}elseif( !$user_ID && $project_status == 'publish'){ ?>
						<a href="#"  class="btn btn-apply-project-item btn-login-trigger" >Place Bid</a>
					<?php } else {
						$role = ae_user_role();
						switch ($project_status) {
							case 'publish':
								if( ( fre_share_role() || $role == FREELANCER ) && $user_ID != $project->post_author ){
									$has_bid = fre_has_bid( get_the_ID() );
									if( $has_bid ) {
										?>
										<a rel="<?php echo $project->ID;?>" href="#" id="<?php echo $has_bid;?>" title= "<?php _e('Cancel this bidding',ET_DOMAIN); ?>"  class="btn btn-apply-project-item btn-del-project" >
											<?php  _e('Cancel',ET_DOMAIN);?>
										</a>
									<?php
									} else{
										// show button bid project
										if ($shop_profile->bank_verification == 'verified') {
											fre_button_bid($project->ID);
										} else {
											echo '<span class="notification-error">You must add a bank account and verify it before placing a bid.</span>';
										}
									}
								}
								break;
							case 'close':
								if( (int)$project->post_author == $user_ID){ ?>

									<a title="<?php  _e('Repairs Complete',ET_DOMAIN);?>" href="#" id="<?php the_ID();?>"   class="btn btn-apply-project-item btn-project-status btn-complete-project" >
										<?php  _e('Repairs Complete',ET_DOMAIN);?>
									</a>
									<a title="<?php _e('Close',ET_DOMAIN);?>" href="#" id="<?php the_ID();?>"   class="btn btn-apply-project-item btn-project-status btn-close-project" >
										<?php _e('Close',ET_DOMAIN);?>
									</a>
									<?php
								}else{
									$bid_accepted_author = get_post_field( 'post_author', $bid_accepted);
									if($bid_accepted_author == $user_ID) {
								?>
									<a title="<?php  _e('Discontinue',ET_DOMAIN);?>" href="#" id="<?php the_ID();?>"   class="btn btn-apply-project-item btn-project-status btn-quit-project" >
										<?php  _e('Discontinue',ET_DOMAIN);?>
									</a>
								<?php }
								}
								break;
							case 'complete' :
								$freelan_id  = (int)get_post_field('post_author',$project->accepted);

								$comment = get_comments( array('status'=> 'approve', 'type' => 'fre_review', 'post_id'=> get_the_ID() ) );

								if( $user_ID == $freelan_id && empty( $comment ) ){ ?>
									<a href="#" id="<?php the_ID();?>"   class="btn btn-apply-project-item btn-project-status btn-complete-project" ><?php  _e('Review & Complete',ET_DOMAIN);?></a>
									<?php
								}
								break;
						}
					}
					?>
					</div>
			</div>

			<?php if(!ae_get_option('disable_project_comment')) { ?>
			<div class="comments" id="project_comment">
				<?php //comments_template('/comments.php', true)?>
			</div>
			<?php } ?>

		</div>
		<div class="col-md-4">
			<div class="content-require-skill-project">
			<?php

				do_action('before_sidebar_single_project', $project);

				//list_tax_of_project( get_the_ID(), __('Skills required:',ET_DOMAIN), 'skill' );
				list_tax_of_project( get_the_ID(), __('Category:',ET_DOMAIN)  );

				// list project attachment
				$attachment = get_children( array(
						'numberposts' => -1,
						'order' => 'ASC',
						'post_parent' => $post->ID,
						'post_type' => 'attachment'
					  ), OBJECT );
				if(!empty($attachment)) {
					echo '<h3 class="title-content">'. __("Attachments:", ET_DOMAIN) .'</h3>';
					echo '<ul class="list-file-attack-report">';
					foreach ($attachment as $key => $att) {
						$file_type = wp_check_filetype($att->post_title, array('jpg' => 'image/jpeg',
																				'jpeg' => 'image/jpeg',
																				'gif' => 'image/gif',
																				'png' => 'image/png',
																				'bmp' => 'image/bmp'
																			)
													);
						$class="text-ellipsis";
						if(isset($file_type['ext']) && $file_type['ext']) $class="image-gallery text-ellipsis";
						echo '<li>
								<a class="'.$class.'" target="_blank" rel="noopener noreferrer"  href="'.$att->guid.'"><i class="fa fa-paperclip"></i>'.$att->post_title.'</a>
							</li>';
					}
					echo '</ul>';
				}
				if(function_exists('et_render_custom_field')) {
					et_render_custom_field($project);
				}

				do_action('after_sidebar_single_project', $project);

			?>

			</div>
		</div>
	</div>
</div>
