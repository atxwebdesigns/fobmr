<?php
/**
 * The template for displaying project detail heading, author info and action button
 */
global $wp_query, $ae_post_factory, $post, $user_ID;
$post_object    = $ae_post_factory->get(PROJECT);
$convert = $project = $post_object->convert($post);

$et_expired_date    = $convert->et_expired_date;
$bid_accepted       = $convert->accepted;
$project_status     = $convert->post_status;
$profile_id         = get_user_meta($post->post_author,'user_profile_id', true);

$currency           = ae_get_option('currency',array('align' => 'left', 'code' => 'USD', 'icon' => '$'));
?>
<div class="col-md-12">
	<div class="tab-content-project">
    	<!-- Title -->
    	<div class="row title-tab-project">
            <div class="col-lg-4 col-md-5 col-sm-4 col-xs-7">
                <span><?php _e("REPAIR TITLE", ET_DOMAIN); ?></span>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-3 hidden-sm hidden-md hidden-xs">
                <span><?php _e("POSTED DATE", ET_DOMAIN); ?></span>
            </div>
        </div>
        <!-- Title / End -->
        <!-- Content project -->
        <div class="single-projects">
            <div class="project type-project project-item">
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-7">
                         <?php if($user_role=="administrator" || $user_role=="employer") { ?>   <a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="avatar-author-project-item">
                            <?php echo get_avatar( $post->post_author, 35,true, get_the_title($profile_id) ); ?>
                     </a><?php  } ?>
                        <h1 class="content-title-project-item"><?php the_title();?></h1>
                    </div>
                   
                    <div class="col-lg-2 col-md-2 col-sm-2 hidden-sm hidden-md hidden-xs">
                         <span  class="time-post-project-item"><?php the_date(); ?></span>
                    </div>

                   
                </div>
            </div>
                <?php if( Fre_ReportForm::AccessReport()
                        && ($post->post_status == 'disputing' || $post->post_status == 'disputed')
                        && !isset($_REQUEST['workspace'])
                    ) { ?>
                    <div class="workplace-container">
                        <?php get_template_part('template/project', 'report') ?>
                    </div>
                <?php }else if( isset($_REQUEST['workspace']) && $_REQUEST['workspace'] ) { ?>
                    <div class="workplace-container">
                        <?php get_template_part('template/project', 'workspaces') ?>
                    </div>
                <?php }else {
                    get_template_part('template/project-detail' , 'info');
                    get_template_part('template/project-detail' , 'content');
                } ?>
            </div>
        <!-- Content project / End -->
        <div class="clearfix"></div>
    </div><!-- tab-content-project !-->
</div>  <!--col-md-12 !-->