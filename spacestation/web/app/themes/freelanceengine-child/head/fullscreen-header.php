<?php
	global $current_user;
	$class_trans = '';
	if(is_page_template('page-home.php')) {
		$class_trans = 'class="transparent"';
	}else{
		$class_trans = 'class="solid"';
	}
	 $user_role = ae_user_role($current_user->ID);
?>
<div class="menu-prime">
	<a href="#close-menu" class="close-menu"></a>
	<?php
		if (has_nav_menu('et_header')) {
			$args = array(
				'theme_location' => 'et_header',
				'menu' => '',
				'container' => 'nav',
				'container_class' => 'menu',
				'container_id' => '',
				'menu_class' => 'menu-main',
				'menu_id' => '',
				'echo' => true,
				'before' => '',
				'after' => '',
				'link_before' => '',
				'link_after' => ''
			);

			wp_nav_menu( $args );
		}
	?>
	<div class="diamond"></div>
</div>
<header <?php echo $class_trans ;?>>
	<a href="#open-menu" class="hamburger">
		<span class="line01"></span>
		<span class="line02"></span>
		<span class="line03"></span>
	</a>
	<a class="logo" href="<?= esc_url(home_url('/')); ?>"></a>
	<div class="user-info">
		<?php
		if (!is_user_logged_in()){ ?>
			<a href="<?= esc_url(home_url('/')); ?>auth" class="login--mobile"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 23" class="undefined"><path d="M16.2 13.3C17.3 12.2 18 10.7 18 9V6c0-3.3-2.7-6-6-6S6 2.7 6 6v3c0 1.7 0.7 3.2 1.8 4.3C3.2 14 0 15.9 0 18v2c0 1.7 1.3 3 3 3h18c1.7 0 3-1.3 3-3v-2C24 15.9 20.7 14 16.2 13.3zM8 9V6c0-2.2 1.8-4 4-4 2.2 0 4 1.8 4 4v3c0 2.2-1.8 4-4 4C9.8 13 8 11.2 8 9zM22 20c0 0.6-0.4 1-1 1H3c-0.6 0-1-0.4-1-1v-2c0-0.2 0.6-1 2.3-1.7 2-0.8 4.8-1.3 7.7-1.3s5.7 0.5 7.7 1.3C21.4 17 22 17.8 22 18V20z"/></svg></a>
			<a href="#login" class="login guest-links login-btn"><?php _e("LOGIN", ET_DOMAIN) ?></a>
			<a href="#sign-up" class="sign-up guest-links register-btn"><?php _e("SIGN UP", ET_DOMAIN) ?></a>
		<?php } else { ?>
			<div class="dropdown-info-acc-wrapper">
				<div class="dropdown et-dropdown">
					<div class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
						<span class="avatar">
							<?php
							$notify_number = 0;
							if(function_exists('fre_user_have_notify') ) {
								$notify_number = fre_user_have_notify();
								if($notify_number) {
									echo '<span class="trigger-overlay trigger-notification-2 circle-new">'.$notify_number.'</span>';
								}
							}
							echo get_avatar($user_ID);
							?>
							<span><?php echo $current_user->display_name; ?></span>
						</span>
						<span class="caret"></span>
					</div>
					<ul class="dropdown-menu et-dropdown-login" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="<?php echo et_get_page_link("profile") ?>" class="display-name">
								<i class="fa fa-user"></i><?php _e("Your Profile", ET_DOMAIN) ?>
							</a>
						</li>
						<?php do_action('fre_header_before_notify'); ?>
						<li role="presentation">
							<a href="javascript:void(0);" class="trigger-overlay trigger-notification">
								<i class="fa fa-flag"></i><?php _e('Notification', ET_DOMAIN);?>
								<?php
									if($notify_number) {
										echo '<span class="notify-number">(' . $notify_number . ')</span>';
									}
								 ?>
							</a>
						</li>
						<?php do_action('fre_header_after_notify'); ?>
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="<?php echo wp_logout_url(); ?>" class="logout">
								<i class="fa fa-sign-out"></i><?php _e("Logout", ET_DOMAIN) ?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		<?php } ?>
	</div>
</header>
