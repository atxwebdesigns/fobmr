<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */
global $current_user;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<?php global $user_ID; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" type="image/png" href="<?php bloginfo("template_url"); ?>-child/dist/images/favicon.png">
	<?php
	wp_head();
	if(function_exists('et_render_less_style')) {
		et_render_less_style();
	}

	// Stripe Congig
	include_once('stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey("sk_test_F8H1BgbvecN12Okw7mL91OYq");
	// -- end config
	?>
</head>

<body <?php body_class(); ?>>
<!-- MENU DOOR -->
<div class="overlay overlay-scale">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<a href="javascript:void(0);" class="overlay-close"><i class="fa fa-times"></i></a>
			</div>
		</div>
	</div>
	<?php get_template_part('head/notification'); ?>
</div>
<!-- MENU DOOR / END -->
<?php
	$class_trans = '';
	if(is_page_template('page-home.php')) {
		$class_trans = 'class="trans-color"';
	}else{
		$class_trans = 'class="not-page-home"';
	}
?>
<!-- HEADER -->
<?php
	get_template_part( 'head/fullscreen', 'header' );
?>
<!-- HEADER / END -->

<?php
if(!is_user_logged_in()){
	get_template_part( 'template-js/header', 'login' );
}
global $user_ID;
if($user_ID) {
//     echo '<script type="data/json"  id="user_id">'. json_encode(array('id' => $user_ID, 'ID'=> $user_ID, 'roles' => $current_user->roles) ) .'</script>';
// }
	echo '<script type="data/json"  id="user_id">'. json_encode(array('id' => $user_ID, 'ID'=> $user_ID) ) .'</script>';
}
