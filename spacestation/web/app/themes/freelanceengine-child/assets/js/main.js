/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

	// HELPERS
	var addAnimations = function (animations, trigger, hook, offset, duration, controller) {
		var scene = '';
		var tween = new TimelineMax();
		tween.insertMultiple(animations, 0, 0);

		if (hook && duration) {
			scene = new ScrollMagic.Scene({
				triggerElement: trigger,
				triggerHook: hook,
				offset: offset,
				duration: duration
			})
					.setTween(tween)
					.addTo(controller);
		} else if (duration) {
			scene = new ScrollMagic.Scene({
				triggerElement: trigger,
				offset: offset,
				duration: duration
			})
					.setTween(tween)
					.addTo(controller);
		} else {
			scene = new ScrollMagic.Scene({
				triggerElement: trigger,
				offset: offset
			})
					.setTween(tween)
					.addTo(controller);
		}

		return;
	};

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Hyperdrive = {
		// All pages
		'common': {
			init: function() {
				// Open Menu
				$('.hamburger').click(function(e) {
					e.preventDefault();
					$('body').css('overflow', 'hidden');
					TweenMax.to(".menu-prime", 0.9, {
						x: "0%", ease: Quint.easeInOut
					});
					TweenMax.fromTo(".menu-prime .close-menu", 0.6, {
						opacity: "0", y: -40}, {
						opacity: "1", y: 0, delay: 0.5, ease: Quint.easeInOut
					});
					TweenMax.fromTo(".menu-prime .diamond", 0.9, {
						opacity: "0", y: "16%", x: "12%", rotation: 20, transformOrign:'50% 100%'}, {
						opacity: "1", y: "0%", x: "0%", rotation: 45, delay: 0.5, ease: Quint.easeInOut
					});
					TweenMax.fromTo(".menu-prime .menu", 1.2, {
						opacity: "0", y: "26%"}, {
						opacity: "1", y: "0%", delay: 0.5, ease: Quint.easeInOut
					});
				});

				// Close Menu
				$('.close-menu').click(function(e) {
					e.preventDefault();
					$('body').css('overflow', 'auto');
					TweenMax.to(".menu-prime .close-menu", 0.6, {
						opacity: "0", y: -40, ease: Quint.easeInOut
					});
					TweenMax.to(".menu-prime .diamond", 0.9, {
						opacity: "0", y: "16%", x: "12%", rotation: 70, ease: Quint.easeInOut
					});
					TweenMax.to(".menu-prime .menu", 1, {
						opacity: "0", y: "-16%", ease: Quint.easeInOut
					});
					TweenMax.to(".menu-prime", 0.9, {
						x: "-100%", delay: 0.2, ease: Quint.easeInOut
					});
				});
			},
			finalize: function() {
				// Navigation
				var stickyHeaderUpdate = stickyHeader('header');
				stickyHeaderUpdate();
				$(window).on('scroll', stickyHeaderUpdate);
				function stickyHeader(header) {
					var sticky = false,
					$header = $(header);
					return function update() {
						if (window.scrollY > 130) {
							if (!sticky) {
								$header.addClass('sticky');
								sticky = true;
							}
						} else {
							if (sticky) {
								$header.removeClass('sticky');
								sticky = false;
							}
						}
					};
				} // stickyHeader()
			}
		},
		// Splash Template
		'page_template_page_splash': {
			init: function() {

				// Animate in logo
				TweenMax.fromTo(".hero-video .logo", 0.9, {
					opacity: "0", x: "-100%"}, {
					opacity: "1", x: "0%", delay: 1, ease: Quint.easeOut
				});
				// Animate in H4
				TweenMax.fromTo(".hero-video h4", 0.6, {
					opacity: "0", y: 70}, {
					opacity: "0.5", y: 0, delay: 1.5, ease: Power1.easeOut
				});
				// Animate in H1
				TweenMax.fromTo(".hero-video h1", 0.6, {
					opacity: "0", y: 70}, {
					opacity: "1", y: 0, delay: 1.9, ease: Power1.easeOut
				});

				// Animate Slices
				TweenMax.fromTo(".hero-video .graphics .frame", 0.5, {
					opacity: "0"}, {
					opacity: "1", delay: 0.4, ease: Power1.easeOut
				});
				TweenMax.fromTo(".hero-video .graphics .top-slice", 1, {
					opacity: "0", y: -150}, {
					opacity: "0.8", y: 0, delay: 1.2, ease: Power3.easeOut
				});
				TweenMax.fromTo(".hero-video .graphics .bottom-slice", 0.7, {
					opacity: "0", y: 150}, {
					opacity: "0.8", y: 0, delay: 1.65, ease: Power3.easeOut
				});
			},
			finalize: function() {
			}
		},
		// Home Page
		'page_template_page_home': {
			init: function() {

				// Hero Sectory Shift
				$('.content__sector01').hover(function() {
					$('.hero__sector01').toggleClass('shift-right');
					$('.hero__sector02').toggleClass('shift-right');
				});

				$('.content__sector02').hover(function() {
					$('.hero__sector02').toggleClass('shift-left');
					$('.hero__sector01').toggleClass('shift-left');
				});

				// ::
				// HERO ITEMS ---------------------------::
				// ____
				TweenMax.fromTo(".hero .content__sector01 h2", 0.6, {
					opacity: "0", y: 60}, {
					opacity: "1", y: 0, delay: 1, ease: Power1.easeOut
				});
				TweenMax.fromTo(".hero .content__sector02 h2", 0.6, {
					opacity: "0", y: -60}, {
					opacity: "1", y: 0, delay: 1, ease: Power1.easeOut
				});
				TweenMax.fromTo(".hero .content__sector01 .button-diagonal", 0.6, {
					opacity: "0", x: -60}, {
					opacity: "1", x: 0, delay: 2, ease: Power1.easeOut
				});
				TweenMax.fromTo(".hero .content__sector02 .button-diagonal", 0.6, {
					opacity: "0", x: 60}, {
					opacity: "1", x: 0, delay: 2, ease: Power1.easeOut
				});


				// ::
				// PAGE SCROLL DOWN ---------------------------::
				// ____
				$('.hero__footer .page-down').on('click', function(event) {
					event.preventDefault();
					var target = $(this).attr('href');
					var destination = $(target).offset().top - 90; //offset the nav
					$('body, html').animate({
						scrollTop: destination + 'px'
					}, 1000);
				});

				// ::
				// HOW IT WORKS PARALLAX ---------------------------::
				// ____
				var parallaxcontrol = new ScrollMagic.Controller();
				var windowHalfX = window.innerHeight / 2;

				addAnimations([
					TweenMax.fromTo(".how-does-it-work .floating-box .info", 2, {
						y: "12%"}, {
						y: "-20%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".how-does-it-work .floating-box .info .heading", 2, {
						y: "15%"}, {
						y: "-10%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".how-does-it-work .floating-box .video-link", 2, {
						y: "10%"}, {
						y: "-18%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".how-does-it-work .diagshape", 2, {
						y: "0"}, {
						y: "-15%", ease: Linear.easeNone
					})
				], '.how-does-it-work', '', -windowHalfX, "180%", parallaxcontrol);

				// ::
				// OPEN VIDEO ---------------------------::
				// ____
				var players = plyr.setup('.js-player');
				$('.video-link').on('click', function(event) {
					event.preventDefault();
					$('body').css('overflow', 'hidden');
					$('.open-video').addClass('opened');
					$('header').addClass('hidden-header');
					players[0].play();
				});

				$('.open-video .close').on('click', function(event) {
					event.preventDefault();
					$('body').css('overflow', 'auto');
					$('.open-video').removeClass('opened');
					$('header').removeClass('hidden-header');
					players[0].pause();
				});

				// ::
				// SLIDER PARALLAX ---------------------------::
				// ____
				addAnimations([
					TweenMax.fromTo(".home-content .slider-switch", 2, {
						y: "50%"}, {
						y: "-20%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".home-content .hiw-slider .image-container", 2, {
						y: "35%"}, {
						y: "-28%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".home-content .hiw-slider .text-container", 2, {
						y: "12%"}, {
						y: "-18%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".home-content .hiw-slider .square__blue", 2, {
						y: "-5%"}, {
						y: "9%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".home-content .hiw-slider .square__red", 2, {
						y: "-2%"}, {
						y: "13%", ease: Linear.easeNone
					}),
					TweenMax.fromTo(".home-content .hiw-slider .slider-controls", 2, {
						y: "100%"}, {
						y: "-50%", ease: Linear.easeNone
					}),
				], '.home-content', '', -windowHalfX, "180%", parallaxcontrol);

				/*
				How to tell if a DOM element is visible in the current viewport?
				*/
				function elementInViewport(el) {
					var top = el.offsetTop;
					var left = el.offsetLeft;
					var width = el.offsetWidth;
					var height = el.offsetHeight;

					while (el.offsetParent) {
						el = el.offsetParent;
						top += el.offsetTop;
						left += el.offsetLeft;
					}

					return (
						top < (window.pageYOffset + window.innerHeight) &&
						left < (window.pageXOffset + window.innerWidth) &&
						(top + height) > window.pageYOffset &&
						(left + width) > window.pageXOffset
					);
				}

				// Switch Sliders
				$(".slider-switch .sswitch-customer").on('click', function(event) {
					event.preventDefault();
					$(this).addClass("switcher--active");
					$(".sswitch-shop").removeClass("switcher--active");

					$("#slider-customer").addClass("visible");
					$("#slider-shop").removeClass("visible");

				});
				$(".slider-switch .sswitch-shop").on('click', function(event) {
					event.preventDefault();
					$(this).addClass("switcher--active");
					$(".sswitch-customer").removeClass("switcher--active");

					$("#slider-customer").removeClass("visible");
					$("#slider-shop").addClass("visible");
				});

				//initialize the slider
				$('.hiw-slider').each(function () {
					initSlider($(this));
				});

				function initSlider(sliderWrapper) {
					//cache jQuery objects
					var slider = sliderWrapper.find('.image-container'),
						sliderNav = sliderWrapper.find('.slider-controls');

					//store path 'd' attribute values
					var pathArray = [];
					pathArray[0] = slider.data('step1');
					pathArray[1] = slider.data('step4');
					pathArray[2] = slider.data('step2');
					pathArray[3] = slider.data('step5');
					pathArray[4] = slider.data('step3');
					pathArray[5] = slider.data('step6');

					//update visible slide when user clicks next/prev arrows
					sliderNav.on('click', '.__next', function (event) {
						event.preventDefault();
						nextSlide(slider, pathArray);
					});
					sliderNav.on('click', '.__prev', function (event) {
						event.preventDefault();
						prevSlide(slider, pathArray);
					});

					//detect swipe event on mobile - update visible slide
					/*slider.on('swipeleft', function (event) {
						nextSlide(slider, pathArray);
					});
					slider.on('swiperight', function (event) {
						prevSlide(slider, pathArray);
					});*/

					//keyboard slider navigation
					$(document).keyup(function (event) {
						if (event.which == '37' && elementInViewport(slider.get(0))) {
							prevSlide(slider, pathArray);
						} else if (event.which == '39' && elementInViewport(slider.get(0))) {
							nextSlide(slider, pathArray);
						}
					});
				} // End initSlider

				function retrieveVisibleSlide(slider, pathArray) {
					return slider.find('.visible');
				}

				function nextSlide(slider, pathArray) {
					var visibleSlide = retrieveVisibleSlide(slider),
						nextSlide = (visibleSlide.next('li').length > 0) ? visibleSlide.next('li') : slider.find('li').eq(0);
					updateSlide(visibleSlide, nextSlide, 'next', pathArray);
				}
				function prevSlide(slider, pathArray) {
					var visibleSlide = retrieveVisibleSlide(slider),
							prevSlide = (visibleSlide.prev('li').length > 0) ? visibleSlide.prev('li') : slider.find('li').last();
					updateSlide(visibleSlide, prevSlide, 'prev', pathArray);
				}

				function updateSlide(oldSlide, newSlide, direction, paths) {
					if (direction == 'next') {
						var path1 = paths[0],
							path2 = paths[2],
							path3 = paths[4];
					} else {
						var path1 = paths[1],
							path2 = paths[3],
							path3 = paths[5];
					}

					oldSlide.removeClass('visible');
					newSlide.addClass('visible');

					var sliderContainer = oldSlide.parents('.hiw-slider');

					sliderContainer.find('.text-field[data-slide-ref="' + oldSlide.attr('data-slide-ref') + '"]').removeClass('visible');
					sliderContainer.find('.text-field[data-slide-ref="' + newSlide.attr('data-slide-ref') + '"]').addClass('visible');

					sliderContainer.find('.slide-counter .current[data-slide-ref="' + oldSlide.attr('data-slide-ref') + '"]').removeClass('visible');
					sliderContainer.find('.slide-counter .current[data-slide-ref="' + newSlide.attr('data-slide-ref') + '"]').addClass('visible');
				}
			},
			finalize: function() {

			}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Hyperdrive;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
