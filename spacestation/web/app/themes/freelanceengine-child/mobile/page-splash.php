<?php
/**
 * Template Name: Splash Page Template
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>-child/dist/css/main.css">
	</head>

	<body class="page-template-page-splash">
		<div class="hero-video">
			<div class="graphics">
				<div class="top-slice"></div>
				<div class="bottom-slice"></div>
				<div class="frame"></div>
			</div>
			<div class="content">
				<img src="<?php bloginfo("template_url"); ?>-child/dist/images/logo.png" class="logo" alt="FOBMR">
				<h4>Fix or Build My Ride</h4>
				<h1>Coming Soon!</h1>
			</div>
		</div>
	<script src="<?php bloginfo("template_url"); ?>-child/dist/js/main.js"></script>
	</body>
</html>
