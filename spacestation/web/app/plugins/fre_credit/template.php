<?php
/**
 * Plugin  template
 * @param void
 * @return void
 * @since 1.0
 * @package FREELANCEENGINE
 * @category PRIVATE MESSAGE
 * @author Jack Bui
*/

if( !function_exists('fre_credit_template_payToSubmitProject_button') ){
	/**
	  * html template for pay to submit project button
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_template_payToSubmitProject_button(){ ?>
		<li class="fre-credit-payment-onsite">
		<span class="title-plan fre-credit-payment" data-type="frecredit">
			<?php
			_e("Your balance", ET_DOMAIN); ?>
			<span><?php
				_e("Send the payment by using your balance", ET_DOMAIN); ?></span>
		</span>
			<a href="#!" class="btn btn-submit-price-plan btn-fre-credit-payment" data-toggle="popover" data-content="<?php _e('Your balance is not enough for this plan', ET_DOMAIN);?>" data-type="frecredit"><?php
				_e("Select", ET_DOMAIN); ?></a>
		</li>
<?php }
}
if( !function_exists('fre_credit_deposit_template') ){
	/**
	  * html template for deposit page
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_deposit_template(){
		if( et_load_mobile() ){
			include dirname(__FILE__) . '/template/fre-credit-deposit.php';
		}
		else {
			include dirname(__FILE__) . '/template/fre-credit-deposit.php';
		}
	}
}
add_shortcode( 'fre_credit_deposit', 'fre_credit_deposit_template' );
if( !function_exists('fre_credit_secure_code_field')) {
	/**
	 * secure code field
	 *
	 * @param void
	 * @return void
	 * @since 1.0
	 * @package FREELANCEENGINE
	 * @category FRE CREDIT
	 * @author Jack Bui
	 */
	function fre_credit_secure_code_field(){ ?>
		<div class="form-group form-secure-code">
			<div class="controls">
				<div class="form-item-left">
					<!-- <label>
						<?php _e('Enter your secure code:', ET_DOMAIN);?>
					</label> -->
					<div class="controls fld-wrap" id="">
						<input tabindex="20" id="fre_credit_secure_code" type="password" size="20" name="fre_credit_secure_code"  class="bg-default-input not_empty" placeholder="<?php _e("Please enter secure code", ET_DOMAIN)?>" />
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
   <?php }
}
if( !function_exists('fre_credit_add_profile_tab') ){
	/**
	  * add tab credit to profile page
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_add_profile_tab(){
		if (ae_user_role() == 'freelancer') { ?>
		<li>
			<a href="#billing" role="tab" data-toggle="tab">
				<?php _e('Billing', ET_DOMAIN) ?>
			</a>
		</li>
   <?php }
	}
}
add_action( 'fre_profile_tabs', 'fre_credit_add_profile_tab',99);
if( !function_exists('fre_credit_add_profile_tab_content') ){
	/**
	  * credit tab content
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_add_profile_tab_content() {
		// global $wp_query, $ae_post_factory, $post, $current_user, $user_ID;
		global $user_ID;
		$user_role = ae_user_role($user_ID);
		$total = fre_credit_get_user_total_balance($user_ID);
		$available = FRE_Credit_Users()->getUserWallet($user_ID);
		$freezable = FRE_Credit_Users()->getUserWallet($user_ID, 'freezable');
		$email_paypal = get_user_meta($user_ID, 'email-paypal-credit', true);
		$banking_info = get_user_meta($user_ID, 'bank-info-credit', true);

		$profile_id = get_user_meta( $user_ID, 'user_profile_id', true);

		if( $user_role == 'freelancer' ) {
			$tooltip_frozen = __('Your withdraw request is waiting for admin approval.', ET_DOMAIN);
		}elseif( $user_role == 'employer' ){
			$tooltip_frozen = __('Your project commission & fee have been sent to admin under Escrow system. Otherwise, your withdraw request is waiting for admin approval.', ET_DOMAIN);
		}else{
			$tooltip_frozen = '';
		}
		$minimum = ae_get_option('fre_credit_minimum_withdraw', 50);
		?>
		<div class="tab-pane fade tabs-credits" id="billing">
			<!-- Infomation Account Credit-->
			<div class="changelog balance">
				<div class="bar-title">
					<p class="title"><?php _e('Account Information', ET_DOMAIN ); ?></p>
				</div>
				<div class="bank-info-credit">
					<?php if (!$profile_id) { ?>
						<p class="notification-error"><i class="fa fa-warning"></i> You must complete your profile 100% before you can enter in your billing info.</p>
					<?php } else { ?>
						<span class="email-bank available"><?php _e('Banking Information:', ET_DOMAIN)?></span>
						<?php if(empty($banking_info)){?>
							<a href="#" class="btn-update-bank" data-toggle="modal" data-target="#" >
								<?php _e('Update', ET_DOMAIN);?>
							</a>
						<?php }else{ ?>
							<span class="budget"><?php echo '********'.substr($banking_info['account_number'],-4); ?></span>
							<a href="#" class="btn-update-bank" data-toggle="modal" data-target="#" >
								<?php _e('Change', ET_DOMAIN);?>
							</a>
						<?php } ?>
					<?php } // end if profile ?>
				</div>
			</div>
			<div class="changelog fre-credit-history-wrapper">
				<div class="changelog-list">
					<div class="paginations-wrapper">
						<?php
						ae_pagination($new_query, get_query_var('paged'), 'page');
						?>
					</div>

					<?php echo '<script type="data/json" class="fre_credit_history_data" >' . json_encode($post_data) . '</script>'; ?>
				</div>
			</div>
		</div>
 <?php  }
}
//add_action( 'fre_profile_tabs_on_mobile', 'fre_credit_add_profile_tab_on_mobile');
add_action( 'fre_profile_tab_content', 'fre_credit_add_profile_tab_content');
if( !function_exists('fre_credit_add_profile_tab_on_mobile') ){
	/**
	  * add tab credit on mobile version
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_add_profile_tab_on_mobile(){ ?>
		<li>
			<a href="<?php echo et_get_page_link('profile'); ?>#tab_credits" class="link-menu-nav">
				<?php _e('Credits', ET_DOMAIN) ?>
			</a>
		</li>
		<li>
			<a href="#" class="request-secure-code">
				<i class="fa fa-key"></i>
				<?php
				global $user_ID;
				if( !FRE_Credit_Users()->getSecureCode($user_ID) ) {
					_e("Request a new Secure Code", ET_DOMAIN);
				}
				else{
					_e("Reset Secure Code", ET_DOMAIN);
				}
				?>
			</a>
		</li>
<?php }
}
add_action( 'fre_profile_tabs_on_mobile', 'fre_credit_add_profile_tab_on_mobile');
if( !function_exists('fre_credit_add_profile_mobile_tab_content') ){
	/**
	  * add credit content to mobile version
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_add_profile_mobile_tab_content(){
		global $user_ID;
		$user_role = ae_user_role($user_ID);
		$total = fre_credit_get_user_total_balance($user_ID);
		$available = FRE_Credit_Users()->getUserWallet($user_ID);
		$freezable = FRE_Credit_Users()->getUserWallet($user_ID, 'freezable');

		$email_paypal = get_user_meta($user_ID, 'email-paypal-credit', true);
		$banking_info = get_user_meta($user_ID, 'bank-info-credit', true);

		if( $user_role == 'freelancer' ) {
			$tooltip_frozen = __('Your withdraw request is waiting for admin approval.', ET_DOMAIN);
		}elseif( $user_role == 'employer' ){
			$tooltip_frozen = __('Your project commission & fee have been sent to admin under Escrow system. Otherwise, your withdraw request is waiting for admin approval.', ET_DOMAIN);
		}else{
			$tooltip_frozen = '';
		}
		$minimum = ae_get_option('fre_credit_minimum_withdraw', 50);
		?>
		<div class="tabs-credits tab-profile mobile-tab-profile" id="tab_credits">

			<!-- Infomation Account Credit-->
			<!-- <div class="changelog balance"> -->
				<div class="top-bar">
				  <span class="text-package "><?php _e('Payment Method', ET_DOMAIN ); ?></span>
				</div>
				<div class="balance">

				<div class="balance-account">
					<span class="email-bank available"><?php _e('Banking Information:', ET_DOMAIN)?></span>
					<?php if(empty($banking_info)){?>
						<a href="#" class="btn-update-bank" data-toggle="modal" data-target="#" >
							<?php _e('Update', ET_DOMAIN);?>
						</a>
					<?php }else{ ?>
						<span class="budget"><?php echo $banking_info['benficial_owner']?></span>
						<a href="#" class="btn-update-bank" data-toggle="modal" data-target="#" >
							<?php _e('Change', ET_DOMAIN);?>
						</a>
					<?php } ?>
				</div>
			</div>
			<!-- Infomation Account Credit-->
			<div class="top-bar">
				<span class="text-package "><?php _e('Credit balance', ET_DOMAIN); ?></span>
				<span class="text-price fre_credit_total_text"><?php echo fre_price_format($total->balance) ?></span>
			</div>
			<div class="balance">
				<div class="row">
					<div class="col-xs-6 available">
						<p><?php _e('Available balance', ET_DOMAIN ) ?></p>
						<p class="price fre_credit_available_text"><?php echo fre_price_format($available->balance) ?></p>
					</div>
					<div class="col-xs-6 frozen">
						<p><?php _e('Frozen balance', ET_DOMAIN); ?></p>
						<p class="price fre_credit_freezable_text"><?php echo fre_price_format($freezable->balance) ?></p>
					</div>
				</div>
			</div>
			<div class="button-functions <?php echo ((float)$minimum > $available->balance) ? 'not-money': ''; ?>">
				<a href="<?php echo fre_credit_deposit_page_link() ?>" class="btn-recharge"><?php _e('Recharge', ET_DOMAIN); ?></a>
				<?php if((float)$minimum > $available->balance):?>
					<a href="#" class="btn-withdraw btn-not-money" data-toggle="popover" data-content="<?php printf(__('Unable to withdraw money. Your available credit must be greater or equal to %s', ET_DOMAIN), ae_get_option('fre_credit_minimum_withdraw', 50)); ?>"><?php _e('Withdraw', ET_DOMAIN); ?></a>
				<?php else:?>
					<a href="#" class="btn-withdraw btn-withdraw-action"><?php _e('Withdraw', ET_DOMAIN); ?></a>
				<?php endif;?>
			</div>
			<div class="change-log fre-credit-history-wrapper">
			  <div class="function-filter">
				  <div class="top-bar">
					  <span class="text-package"><?php _e('Transaction History', ET_DOMAIN); ?></span>
				  </div>
				  <div class="value">
					  <span class="dropbox"><span><?php _e('All transaction', ET_DOMAIN); ?></span> <i class="fa fa-angle-down"></i></span>
					  <ul class="list-value hide fre-credit-history-filter">
						  <li><a href="#" data-value="" class="text-blue-dark" data-name="<?php _e('All Transaction', ET_DOMAIN); ?>"><?php _e('All Transaction', ET_DOMAIN); ?></a></li>
						  <li><a href="#" class="text-blue-light" data-value="deposit" data-name="<?php _e('Deposit', ET_DOMAIN); ?>"><?php _e('Deposit', ET_DOMAIN); ?></a></li>
						  <li><a href="#" class="text-green-dark" data-value="withdraw" data-name="<?php _e('Withdraw', ET_DOMAIN); ?>"><?php _e('Withdraw', ET_DOMAIN); ?></a></li>
						  <li><a href="#" class="text-blue-light" data-value="transfer" data-name="<?php _e('Receive', ET_DOMAIN); ?>"><?php _e('Receive', ET_DOMAIN); ?></a></li>
						  <li><a href="#" class="text-orange-dark" data-value="charge" data-name="<?php _e('Paid', ET_DOMAIN); ?>"><?php _e('Paid', ET_DOMAIN); ?></a></li>

					  </ul>
				  </div>
				  <div class="line-white"></div>
				  <div class="date">
					  <span><?php _e('From', ET_DOMAIN); ?></span>
					  <input type='text' class="" id='fre_credit_from' placeholder="--/--/----"/>
					  <span><?php _e('to', ET_DOMAIN); ?></span>
					  <input type='text' class="" id='fre_credit_to' placeholder="--/--/----"/>
				  </div>
				  <div class="change-log-list">
					  <ul class="list-histories">
						  <?php
						  global $post,$wp_query, $ae_post_factory;
						  $args = array(
							  'post_type'=> 'fre_credit_history',
							  'post_status'=> 'publish',
							  'paged'=>1,
							  'author'=> $user_ID
						  );
						  $new_query = new WP_Query($args);
						  $post_data = array();
						  if( $new_query->have_posts() ):
							  while( $new_query->have_posts() ):
								  $new_query->the_post();
								  $his_obj = $ae_post_factory->get('fre_credit_history');
								  $convert = $his_obj->convert($post);
								  $post_data[]= $convert;
								  include dirname(__FILE__) . '/template/fre-credit-history-item.php';
							  endwhile;
						  else:
							  _e("<li class='no-transaction'><span>There isn't any transaction!</span></li>", ET_DOMAIN);
						  endif;?>
					  </ul>

					  <div class="paginations-wrapper">
						  <?php
						  ae_pagination($new_query, get_query_var('paged'), 'load');
						  ?>
					  </div>

					  <?php echo '<script type="data/json" class="fre_credit_history_data" >' . json_encode($post_data) . '</script>'; ?>
				  </div>
			  </div>
			</div>
		</div>

<?php }
}
add_action('fre_profile_mobile_tab_content', 'fre_credit_add_profile_mobile_tab_content');
if( !function_exists('fre_credit_modal_withdraw') ){
	/**
	  * modal withdraw
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_modal_withdraw(){ ?>
		<!--Show modal-->
		<div class="modal fade withdraw" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<p class="warning"></p>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
						<h4 class="modal-title small-modal-title" id="myModalLabel"><?php _e('Credit withdraw', ET_DOMAIN);?></h4>
					</div>
					<div class="modal-body">
						<form id="fre_credit_withdraw_form" >
							<div class="balance-withdraw">
								<div class="current">
									<span><?php _e('Current total credit', ET_DOMAIN) ; ?></span>
									<span class="price text-blue-light fre_credit_total"><?php echo  fre_price_format(0); ?></span>
								</div>
								<div class="available">
									<span><?php _e('Available credit', ET_DOMAIN); ?></span>
									<span class="price text-green-dark fre_credit_available"><?php echo  fre_price_format(0); ?></span>
								</div>
								<div class="frozen">
									<span><?php _e('Frozen credit', ET_DOMAIN); ?></span>
									<span class="price fre_credit_freezable"><?php echo  fre_price_format(0); ?></span>
								</div>
							</div>
							<div class="amount-withdraw">
								<p class="title"><?php _e('Withdraw amount', ET_DOMAIN); ?> </p>
								<p class="price-amount"><?php _e('Minimum amount is', ET_DOMAIN); ?> <span class="fre_credit_min_withdraw"><?php fre_price_format(0)?></span></p>
								<div class="input-amount">
									<input type="number" name="amount" step="any" value="">
								</div>
							</div>
							<?php
							global $user_ID;
							$email_paypal = get_user_meta($user_ID, 'email-paypal-credit', true);
							$banking_info = get_user_meta($user_ID, 'bank-info-credit', true);
							if(empty($email_paypal) && empty($banking_info)){
							?>
								<div class="payment">
									<label><?php _e('Payment information', ET_DOMAIN); ?></label>
									<textarea name="payment_info"></textarea>
								</div>
							<?php }else{ ?>
							<div class="payment">
								<label><?php _e('Payment method', ET_DOMAIN); ?></label>
								<select name="payment_method" required style="float: right;width: 123px;">
									<?php
									if(!empty($email_paypal)){
										echo "<option value='email-paypal-credit'>".__('Paypal', ET_DOMAIN)."</option>";
									}
									if(!empty($banking_info)){
										echo "<option value='bank-info-credit'>".__('Bank', ET_DOMAIN)."</option>";
									}
									?>
								</select>
							</div>
							<div class="payment">
								<label><?php _e('Note', ET_DOMAIN); ?></label>
								<textarea name="payment_info"></textarea>
							</div>
							<?php } ?>
							<?php if(ae_get_option('fre_credit_secure_code', true)): ?>
							<div class="amount-withdraw security-code">
								<p class="title"><?php _e('Security code', ET_DOMAIN); ?></p>
								<p class="price-amount"><?php _e('Please enter the code that we provided you when register.', ET_DOMAIN); ?></p>
								<div class="input-amount">
									<input type="password" name="secureCode" value="">
								</div>
							</div>
							<?php endif; ?>
							<button type="submit" class="btn-sumary blue-light box-shadow-button-blue btn-submit "><?php _e('Submit', ET_DOMAIN); ?></button>
						</form>
					</div>
				</div>
			</div>
		</div>
<?php }
}

if(!function_exists('fre_credit_modal_update_paypal')){
	/**
	  * modal edit Email Paypal
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author ThanhTu
	  */
	function fre_credit_modal_update_paypal(){
	?>
		<div class="modal fade email_paypal" id="modalEditPaypal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
						<h4 class="modal-title small-modal-title" id="myModalLabel"><?php _e('Update Paypal account information', ET_DOMAIN);?></h4>
					</div>
					<div class="modal-body">
						<form id="fre_credit_edit_paypal_form" >
						  <div class="form-group">
							  <label for="credit_email_paypal"><?php _e('Paypal Account', ET_DOMAIN) ?></label>
							  <input type="text" class="form-control" id="email_paypal" name="email_paypal" placeholder="<?php _e('Enter your email paypal', ET_DOMAIN) ?>">
						  </div>
						  <?php if(ae_get_option('fre_credit_secure_code', true)): ?>
						  <div class="form-group">
							  <label for="verify_secure_code"><?php _e('Secure Code', ET_DOMAIN) ?></label>
							  <input type="text" class="form-control" id="secure_code" name="secure_code" placeholder="<?php _e('Enter secure code', ET_DOMAIN) ?>">
						  </div>
						  <?php endif; ?>
						  <button type="submit" class="btn-sumary blue-light box-shadow-button-blue btn-submit">
							  <?php _e('Update', ET_DOMAIN); ?>
						  </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
}

if (!function_exists('fre_credit_modal_update_bank')) {
	/**
	  * modal edit BANK
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author ThanhTu
	  */
	function fre_credit_modal_update_bank() {
		global $wp_query, $ae_post_factory, $post, $current_user, $user_ID;
		$ae_users  = AE_Users::get_instance();
		$user_data = $ae_users->convert($current_user->data);
		$user_role = ae_user_role($current_user->ID);
		//convert current profile
		$post_object = $ae_post_factory->get(PROFILE);

		$profile_id = get_user_meta( $user_ID, 'user_profile_id', true);

		$profile = array('id' => 0, 'ID' => 0);
		if($profile_id) {
			$profile_post = get_post( $profile_id );
			if($profile_post && !is_wp_error( $profile_post )){
				$profile = $post_object->convert($profile_post);
			}
		}
	?>
		<div class="modal fade email_paypal" id="modalUpdateBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
						<h4 class="modal-title small-modal-title" id="myModalLabel"><?php _e('Update Account Billing', ET_DOMAIN);?></h4>
					</div>
					<div class="modal-body">
						<form id="fre_credit_updat_bank_form" method="POST">
							<div class="form-group account-plan">
							<?php
							if ($profile->account_plan) {
								if ($profile->account_plan == 'feebased') {
									$feebased = 'checked';
									$monthlybased = '';
									$currentplan = '<p class="currentplan">You are currently on the <strong>Basic Plan</strong></p><br>';
								} else if ($profile->account_plan == 'monthlybased') {
									$feebased = '';
									$monthlybased = 'checked';
									$currentplan = '<p class="currentplan">You are currently on the <strong>Premium Plan</strong></p><br>';
								}
							} else {
								$feebased = 'checked';
								$monthlybased = '';
							} ?>
								<h4>ACCOUNT PLAN:</h4>
								<?php echo $currentplan; ?>
								<input type="radio" name="account_plan" id="account_plan" value="feebased" <?php echo $feebased; ?>> <strong>Basic Plan</strong>: 5% fee on every repair<br>
								<input type="radio" name="account_plan" id="account_plan" value="monthlybased" <?php echo $monthlybased; ?>> <strong>Premium Plan</strong>: $20 /month
							</div>
						  <div class="form-group">
							  <h4>BANK INFO:</h4>
							  <label for="benficial_owner"><?php _e('Name on Account', ET_DOMAIN) ?></label>
							  <input type="text" class="form-control" id="benficial_owner" name="benficial_owner" >
						  </div>
							<div class="form-group">
							  <label for="account_phone_number"><?php _e('Phone Number', ET_DOMAIN) ?></label>
							  <input type="text" class="form-control" id="account_phone_number" name="account_phone_number">
						  </div>
							<div class="form-group">
								<label for="routing_number"><?php _e('Routing Number', ET_DOMAIN) ?></label>
								<input type="text" class="form-control" id="routing_number" name="routing_number">
							</div>
							<div class="form-group">
								<label for="account_number"><?php _e('Account number', ET_DOMAIN) ?></label>
								<input type="text" class="form-control" id="account_number" name="account_number">
							</div>
							<div class="bank-errors"></div>
							<?php if ($profile->stripe_customer) {
								if ($profile->bank_verification == 'verified') { ?>
									<div class="form-group">
										<h4>Status: <span class="bstatus--verified">VERIFIED</span></h4>
										<h5>Congratulations!!!</h5>
										You are all set to use FixOrBuildMyRide.com to bid repair jobs and find customers for your shop you have not found before.
									</div>
								<?php } else { ?>
									<div class="form-group">
										<h4>Status: <span class="bstatus--unverified">UNVERIFIED</span></h4>
										After entering your account data choose the "update" button below. The verification amounts will appear in your bank account statement with in 24 to 48 hours. Enter these verification amounts as shown in your bank account. After you enter the amounts, your account is verified and you may begin bidding on repairs!
									</div>
									<div class="form-group">
										<label>Micro Deposits</label>
										<div class="input-wrapper">
											<input type="text" class="form-control input-element" id="verifyBank01" name="verifyBank01" placeholder="Deposit Amount 01">
											<input type="text" class="form-control input-element" id="verifyBank02" name="verifyBank02" placeholder="Deposit Amount 02">
										</div>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div class="form-group banking-information">
									After you've submitted your bank information, we will make two small deposits to your account within 3 business days. When you've received those deposits, come back to your profile to enter the amounts and verify your account and complete your profile.
								</div>
							<?php } ?>
						  <button type="submit" class="btn-sumary blue-light btn-submit">
							  <?php _e('Update', ET_DOMAIN); ?>
						  </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
}

if( !function_exists('fre_credit_add_template') ){
	/**
	  * add template to footer
	  *
	  * @param void
	  * @return void
	  * @since 1.0
	  * @package FREELANCEENGINE
	  * @category FRE CREDIT
	  * @author Jack Bui
	  */
	function fre_credit_add_template(){
		fre_credit_modal_update_bank();
		fre_credit_modal_update_paypal();
		fre_credit_modal_withdraw();
		include_once dirname(__FILE__) . '/template/fre-credit-history-item-js.php';
	}
}
add_action('wp_footer', 'fre_credit_add_template');
if( !function_exists('fre_credit_add_request_secure_code') ) {
	/**
	 * add secure code
	 *
	 * @param void
	 * @return void
	 * @since 1.0
	 * @package FREELANCEENGINE
	 * @category FRE CREDIT
	 * @author Jack Bui
	 */
	function fre_credit_add_request_secure_code(){ ?>
		<li>
			<a href="#" class="request-secure-code">
				<i class="fa fa-key"></i>
				<?php
					global $user_ID;
					if( !FRE_Credit_Users()->getSecureCode($user_ID) ) {
						_e("Request a new Secure Code", ET_DOMAIN);
					}
					else{
						_e("Reset Secure Code", ET_DOMAIN);
					}
				?>
			</a>
		</li>

	<?php }
}
add_action('fre-profile-after-list-setting', 'fre_credit_add_request_secure_code');
