<?php 
	$options = AE_Options::get_instance();
    // save this setting to theme options
    $website_logo = $options->site_logo;
?>
<style type="text/css">
	#stripe_modal .modal-header .close  {
		width: 30px;
		height: 30px;
	}
</style>
<div class="modal fade modal-stripe" id="stripe_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		<?php //if( !function_exists('et_load_mobile') || !et_load_mobile() ) { ?>
			<div class="modal-header">
				<button  style="z-index:1000;" data-dismiss="modal" class="close"><i class="fa fa-times"></i></button>				
				<div class="info slogan">
	      			<h4 class="modal-title"><span class="plan_name">{$plan_name}</span></h4>
	      			<span class="plan_desc">{$plan_description}</span>      
	    		</div>
			</div>
		<?php //} ?>
			<div class="modal-body">
				
				<form class="modal-form" id="stripe_form" novalidate="novalidate" autocomplete="on" data-ajax="false">
					<div class="content">
						<label><?php _e('Name on card',ET_DOMAIN);?></label>
					  	<div class="controls">
							<input tabindex="23" name="" id="name_card"  data-stripe="name" class="bg-default-input not_empty" type="text" />
					 	</div>
					 	<div class="row">
					 		<div class="col-sm-8">
					 			<label><?php _e('Card number', ET_DOMAIN);?></label>
								<div class="controls">
									<input tabindex="20" id="stripe_number" type="text" size="20"  data-stripe="number" class="bg-default-input not_empty" placeholder="&bull;&bull;&bull;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&bull;&bull;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&bull;&bull;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&bull;&bull;&bull;"/>
								</div>
					 		</div>
					 		<div class="col-sm-4">
					 			<label><?php _e('Expiry date', ET_DOMAIN);?></label>
					 			<div class="row">
					 				<div class="col-sm-5 col-xs-6" style="padding-right: 0;">
					 					<input tabindex="21" type="text" size="2" data-stripe="exp-month" placeholder="MM"  class="bg-default-input not_empty" id="exp_month"/>
					 				</div>
					 				<div class="col-sm-7 col-xs-6">
					 					<input tabindex="22" type="text" size="4" data-stripe="exp-year" placeholder="YY"  class="bg-default-input not_empty" id="exp_year"/>
					 				</div>
					 			</div>
					 		</div>
					 	</div>

					 	<label><?php _e('Card code', ET_DOMAIN);?></label>
					 	<div class="controls">
							<input tabindex="24" type="text" size="3"  data-stripe="cvc" class="bg-default-input not_empty input-cvc " placeholder="CVC" id="cvc" />
					  	</div>
					</div>
					<div class="button">  
						<button class="btn-sumary orange-dark box-shadow-button-orange btn-submit " type="submit"  id="submit_stripe"><?php _e('Make Payment',ET_DOMAIN);?> </button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-close"></div>
</div>